<?php

namespace Jaguero\MezCalendarBundle\Controller;

use Jaguero\MezCalendarBundle\Entity\Event;
use Jaguero\MezCalendarBundle\Form\Type\EventType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\QueryBuilder;

/**
 * Event controller.
 *
 * @Route("/admin/mezcalendar/event")
 */
class AdminEventController extends Controller
{
    /**
     * Lists all Event entities.
     *
     * @Route("/", name="admin_mezcalendar_event")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository(Event::class)->createQueryBuilder('e');
        $this->addQueryBuilderSort($qb, 'event');
        $paginator = $this->get('knp_paginator')->paginate($qb, $request->query->get('page', 1), 20);

        return array(
            'paginator' => $paginator,
        );
    }

    /**
     * Finds and displays a Event entity.
     *
     * @Route("/{id}/show", name="admin_mezcalendar_event_show", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction(Event $event)
    {
        $editForm = $this->createForm(new EventType(), $event, array(
            'action' => $this->generateUrl('admin_mezcalendar_event_update', array('id' => $event->getid())),
            'method' => 'PUT',
        ));
        $deleteForm = $this->createDeleteForm($event->getId(), 'admin_mezcalendar_event_delete');

        return array(
            'event' => $event, 'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Event entity.
     *
     * @Route("/new", name="admin_mezcalendar_event_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $event = new Event();
        $form = $this->createForm(new EventType(), $event);

        return array(
            'event' => $event,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new Event entity.
     *
     * @Route("/create", name="admin_mezcalendar_event_create")
     * @Method("POST")
     * @Template("AmulenCalendarBundle:Event:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $event = new Event();
        $form = $this->createForm(new EventType(), $event);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_mezcalendar_event_show', array('id' => $event->getId())));
        }

        return array(
            'event' => $event,
            'form' => $form->createView(),
        );
    }

    /**
     * Edits an existing Event entity.
     *
     * @Route("/{id}/update", name="admin_mezcalendar_event_update", requirements={"id"="\d+"})
     * @Method("PUT")
     * @Template("AmulenCalendarBundle:Event:edit.html.twig")
     */
    public function updateAction(Event $event, Request $request)
    {
        $editForm = $this->createForm(new EventType(), $event, array(
            'action' => $this->generateUrl('admin_mezcalendar_event_update', array('id' => $event->getid())),
            'method' => 'PUT',
        ));
        if ($editForm->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('admin_mezcalendar_event_show', array('id' => $event->getId())));
        }
        $deleteForm = $this->createDeleteForm($event->getId(), 'admin_mezcalendar_event_delete');

        return array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     * Save order.
     *
     * @Route("/order/{field}/{type}", name="admin_mezcalendar_event_sort")
     */
    public function sortAction($field, $type)
    {
        $this->setOrder('event', $field, $type);

        return $this->redirect($this->generateUrl('admin_mezcalendar_event'));
    }

    /**
     * @param string $name session name
     * @param string $field field name
     * @param string $type sort type ("ASC"/"DESC")
     */
    protected function setOrder($name, $field, $type = 'ASC')
    {
        $this->getRequest()->getSession()->set('sort.' . $name, array('field' => $field, 'type' => $type));
    }

    /**
     * @param  string $name
     * @return array
     */
    protected function getOrder($name)
    {
        $session = $this->getRequest()->getSession();

        return $session->has('sort.' . $name) ? $session->get('sort.' . $name) : null;
    }

    /**
     * @param QueryBuilder $qb
     * @param string $name
     */
    protected function addQueryBuilderSort(QueryBuilder $qb, $name)
    {
        $alias = current($qb->getDQLPart('from'))->getAlias();
        if (is_array($order = $this->getOrder($name))) {
            $qb->orderBy($alias . '.' . $order['field'], $order['type']);
        }
    }

    /**
     * Deletes a Event entity.
     *
     * @Route("/{id}/delete", name="admin_mezcalendar_event_delete", requirements={"id"="\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Event $event, Request $request)
    {
        $form = $this->createDeleteForm($event->getId(), 'admin_mezcalendar_event_delete');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_mezcalendar_event'));
    }

    /**
     * Create Delete form
     *
     * @param integer $id
     * @param string $route
     * @return \Symfony\Component\Form\Form
     */
    protected function createDeleteForm($id, $route)
    {
        return $this->createFormBuilder(null, array('attr' => array('id' => 'delete')))
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod('DELETE')
            ->getForm();
    }

}
