<?php

namespace Jaguero\MezCalendarBundle\Controller;

use Jaguero\MezCalendarBundle\Entity\Event;
use Jaguero\MezCalendarBundle\Form\Type\EventType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\QueryBuilder;

/**
 * Widget controller.
 */
class WidgetController extends Controller
{
    /**
     * Show next events.
     *
     * @Template()
     */
    public function nextAction($max = 10)
    {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository(Event::class)->findNextEvents();

        return array(
            'events' => $events,
        );
    }


}
