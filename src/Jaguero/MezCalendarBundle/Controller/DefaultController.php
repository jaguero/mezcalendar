<?php

namespace Jaguero\MezCalendarBundle\Controller;

use Jaguero\MezCalendarBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/admin/calendar")
     * @Template()
     */
    public function adminAction()
    {
        return array();
    }

    /**
     * @Route("/calendar/events", name="mezcalendar_events")
     */
    public function getAllAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository(Event::class)->findAll();

        $eventsArr = [];

        /* @var Event $event */
        foreach ($events as $event) {
            $eventsArrItem = [
                'title' => $event->getTitle(),
                'start' => $event->getStart()->format('c'),
                'url' => $this->generateUrl('mezcalendar_event_view', [
                    'slug' => $event->getSlug(),
                ]),
                'hasTime' => true,
            ];
            array_push($eventsArr, $eventsArrItem);
        }

        return new JsonResponse($eventsArr);
    }

    /**
     * @Route("/event/{slug}", name="mezcalendar_event_view")
     * @Template("JagueroMezCalendarBundle:Default:event.html.twig")
     */
    public function viewAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var Event $event */
        $event = $em->getRepository(Event::class)->findOneBy(array("slug" => $slug));

        if (!$event) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }

        $seoPage = $this->container->get('sonata.seo.page');

        $pageTitle = $event->getTitle() . " - " . $seoPage->getTitle();
        $pageDescription = $event->getAbstract();

        $baseUrl = $this->container->getParameter('router.request_context.scheme') . "://";
        $baseUrl .= $this->container->getParameter('router.request_context.host');
        $baseUrl .= $this->container->getParameter('router.request_context.base_url');

        $seoPage
            ->setTitle($pageTitle)
            ->addMeta('name', 'description', $pageDescription)
            ->addMeta('property', 'og:title', $event->getTitle())
            ->addMeta('property', 'og:description', $pageDescription)
            ->addMeta('property', 'og:image', $baseUrl . $event->getImage());

        return [
            'event' => $event,
        ];
    }
}
