<?php

namespace Jaguero\MezCalendarBundle\Event;

use Flowcode\DashboardBundle\Event\ListPluginsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;


/**
 * Created by PhpStorm.
 * User: juanma
 * Date: 5/28/16
 * Time: 12:20 PM
 */
class ListPluginsSubscriber implements EventSubscriberInterface
{
    protected $router;
    protected $translator;

    public function __construct(RouterInterface $router, TranslatorInterface $translator)
    {
        $this->router = $router;
        $this->translator = $translator;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            ListPluginsEvent::NAME => array('handler', 0),
        );
    }


    public function handler(ListPluginsEvent $event)
    {
        $pluginList = $event->getPluginDescriptors();

        /* add default */
        $plugin = [
            "name" => "MezCalendar",
            "image" => "",
            "version" => "0.1",
            "settings" => '#',
            "description" => $this->translator->trans('plugin.description', [], 'mezcalendar'),
            "website" => null,
            "authors" => array(
                array(
                    "name" => "Juan Manuel Aguero",
                    "email" => "jaguero@flowcode.com.ar",
                    "website" => "http://juanmaaguero.com",
                ),
            ),
        ];

        array_push($pluginList, $plugin);

        $event->setPluginDescriptors($pluginList);

    }
}