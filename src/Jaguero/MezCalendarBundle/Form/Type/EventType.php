<?php

namespace Jaguero\MezCalendarBundle\Form\Type;

use Jaguero\MezCalendarBundle\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('image')
            ->add('allDay')
            ->add('start')
            ->add('end')
            ->add('address', null, [
                'attr' => ['class' => 'auto-address']
            ])
            ->add('apartment')
            ->add('country')
            ->add('state')
            ->add('city')
            ->add('postalCode')
            ->add('latitude')
            ->add('longitude')
            ->add('abstract')
            ->add('content', 'ckeditor')
            ->add('enabled');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Event::class,
            'translation_domain' => 'Event',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'event';
    }
}
