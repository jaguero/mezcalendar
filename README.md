# MezCalendar
Calendar for AmulenCMS

## Install

Require with composer:

```
  composer require jaguero/mezcalendar
```

Run Amulen install command:
```
  php app/console amulen:plugin:register "Jaguero\MezCalendarBundle\JagueroMezCalendarBundle"
```

Update DB schema:
```
  php app/console doctrine:schema:update --force
```

## Widgets